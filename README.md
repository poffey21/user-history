
## User History

Given a group, list all of the users from the Audit API and 
show when the add action was granted.

This same information can be found in the Group's Settings > Audit Events.

## How to use

1. Fork this repository
1. Run a pipeline by providing two environment variables
   * PRIVATE_TOKEN - which is a private access token of an owner or maintainer of the group
   * GROUP - which is the group ID of the parent group
