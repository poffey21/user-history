import requests
from pprint import pprint
from envparse import Env
import os

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')

def get_audit_events(url, headers, page_num=1):
    events = {}
    r = requests.get(url, headers=headers)
    while r.ok and isinstance(r.json(), list) and len(r.json()) > 0:
        for event in r.json():
            if event.get('details', {}).get('add', '') != 'user_access':
                continue
            events[event['details']['target_details']] = event['created_at']
            if event['details']['target_details'] == 'Tim Poffenbarger':
                print(event)
        page_num += 1 
        r = requests.get(f'{url}?page={page_num}', headers=headers)
    return events

def main():
    origin = 'https://gitlab.com/api/v4'
    namespace = env('GROUP')
    headers = {'PRIVATE-TOKEN': env('PRIVATE_TOKEN')}
    url = f'{origin}/groups/{namespace}/audit_events/'
    events = get_audit_events(url, headers)
    pprint(events)


if __name__ == '__main__':
    main()

